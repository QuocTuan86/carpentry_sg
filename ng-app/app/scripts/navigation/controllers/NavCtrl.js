angular.module('myApp.navigation.controllers', ['myApp.navigation.services'])
  .controller('NavCtrl', ['$scope', '$window', 'NavInit', function ($scope, $window, NavInit) {
    
    $(window).resize(function(){
      NavInit.init_classic_menu_resize();
    });

    NavInit.init_classic_menu();

    $(window).trigger("resize");
    
  }]);
