angular.module('myApp')
  .controller('MainCtrl', ['$scope', '$rootScope', '$templateCache', '$location', function ($scope, $rootScope, $templateCache, $location) {

    $(window).load(function(){
        
        // Page loader
        $(".page-loader b").delay(0).fadeOut();
        $(".page-loader").delay(200).fadeOut("slow");
        
        $(window).trigger("scroll");
        $(window).trigger("resize");
        
    });

    $rootScope.$on('$viewContentLoaded', function() {
      $templateCache.removeAll();
    });

    $rootScope.$on('$routeChangeStart', function(event, current, previous, rejection){
      if( current.$$route.originalPath == '/pricing' ||
          current.$$route.originalPath == '/how-it-works' ||
          current.$$route.originalPath == '/information' ||
          current.$$route.originalPath == '/terms' ){
        $('.bannercontainer').fadeOut(200);
        $('body').removeClass('body-bg-dark');
        $('body').addClass('body-bg-white');
      }
      if( current.$$route.originalPath == '/' ||
          current.$$route.originalPath == '/about' ||
          current.$$route.originalPath == '/portfolio-commercial' ||
          current.$$route.originalPath == '/portfolio-residential' ||
          current.$$route.originalPath == '/contact' ||
          current.$$route.originalPath == '/career'
        ) {
        $('body').removeClass('body-bg-white');
        $('body').addClass('body-bg-dark');
      }
    })
    
    $rootScope.$on('$routeChangeSuccess', function(event, current, previous, rejection){
      $('body').animate({ scrollTop: 0 })
    })

  }]);
