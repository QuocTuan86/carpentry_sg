angular.module('myApp.page.services')
  .factory('Parallax', ['PlatformDetect', function(PlatformDetect){

    var init = function() {
      // Parallax        
      if (($(window).width() >= 1024) && (PlatformDetect.mobileTest() == false)) {
        $(".parallax-1").parallax("50%", 0.1);
        $(".parallax-2").parallax("50%", 0.2);
        $(".parallax-3").parallax("50%", 0.3);
        $(".parallax-4").parallax("50%", 0.4);
        $(".parallax-5").parallax("50%", 0.5);
        $(".parallax-6").parallax("50%", 0.6);
        $(".parallax-7").parallax("50%", 0.7);
        $(".parallax-8").parallax("50%", 0.5);
        $(".parallax-9").parallax("50%", 0.5);
        $(".parallax-10").parallax("50%", 0.5);
        $(".parallax-11").parallax("50%", 0.05);
      }
    }

    return { init: init };
  }]);