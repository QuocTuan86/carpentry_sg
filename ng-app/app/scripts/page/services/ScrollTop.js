angular.module('myApp.page.services')
  .factory('ScrollTop', function(){

    var init = function(){
      
      (function($){

        // Open/Close map        
        $("#scroll-top").click(function(){
            
            setTimeout(function(){
                $("html, body").animate({
                    scrollTop: $('body').offset().top
                }, 3000, "easeOutBack");
            }, 100);
            
            return false;
          });
      })(jQuery);

    }

    return {
      init: init
    };

  });