angular.module('myApp.page.services')
  .factory('Service', function(){

    var init = function(){

      var service_item = $(".service-item");
      var service_descr = service_item.find(".service-descr");
      var service_descr_top;
      

      (function($){
    
        // $(".service-item").each(function(){
        //     $(this).find(".service-descr").prepend($(this).find(".service-intro").html());
        // });
        
        // Hover        
        service_item.click(function(){
            if ($("html").hasClass("mobile")) {
                if ($(this).hasClass("js-active")) {
                    $(this).removeClass("js-active");
                }
                else {
                    $(this).addClass("js-active");
                }
            }
        });
        
      })(jQuery);
    }

    return {
      init: init
    };

  });