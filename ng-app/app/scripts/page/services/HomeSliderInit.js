angular.module('myApp.page.services')
  .factory('HomeSliderInit', function(){
    
    var initPageSliders = function(){
      (function($){
        "use strict";
        
        jQuery('.fullscreenbanner').revolution({
         delay:5000,
         startwidth: window.innerWidth,
         startheight: window.innerHeight,
         startWithSlide:0,
 
         fullScreenAlignForce:"on",
         autoHeight:"on",
 
         shuffle:"off",
 
         onHoverStop:"off",
 
         hideThumbsOnMobile:"off",
         hideNavDelayOnMobile:1500,
         hideBulletsOnMobile:"off",
         hideArrowsOnMobile:"off",
         hideThumbsUnderResoluition:0,
 
         hideThumbs:0,
         hideTimerBar:"off",
 
         keyboardNavigation:"off",
 
         navigationType:"none",
         navigationArrows:"none",
         navigationStyle:"none",
 
         touchenabled:"off",
         swipe_velocity:"0.7",
         swipe_max_touches:"1",
         swipe_min_touches:"1",
         drag_block_vertical:"false",
 
         parallax:"off",
         parallaxBgFreeze:"on",
         parallaxLevels:[10,7,4,3,2,5,4,3,2,1],
         parallaxDisableOnMobile:"off",
 
         stopAtSlide:-1,
         stopAfterLoops:-1,
         hideCaptionAtLimit:0,
         hideAllCaptionAtLilmit:0,
         hideSliderAtLimit:0,
 
         dottedOverlay:"none",
 
         spinned:"none",
 
         fullWidth:"on",
         forceFullWidth:"on",
         fullScreen:"on",
         fullScreenOffsetContainer:"#topheader-to-offset",
         fullScreenOffset:"0px",
 
         shadow:0
 
      });

      })(jQuery);
    };

    var js_height_init = function(){
      (function($){
          $(".js-height-full").height($(window).height());
          $(".js-height-parent").each(function(){
              $(this).height($(this).parent().first().height());
          });
      })(jQuery);
    }

    return {
      initPageSliders: initPageSliders,
      js_height_init: js_height_init,
    }

  });