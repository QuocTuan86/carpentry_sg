angular.module('myApp.page.services')
  .factory('PortfolioSliderInit', ['$location', function($location){
    
    var initPageSliders = function(slides){

      (function($){
        "use strict";
        
        jQuery('.banner').revolution({
         delay:3000,
         startwidth: window.innerWidth - 200,
         startheight: window.innerHeight - 100,
         startWithSlide:0,
 
         fullScreenAlignForce:"off",
         autoHeight:"on",
 
         shuffle:"off",
 
         onHoverStop:"off",

         thumbWidth:100,
         thumbHeight:75,
         thumbAmount: Math.min( slides.length , Math.floor(960 / 100) ),
 
         hideThumbsOnMobile:"off",
         hideNavDelayOnMobile:1500,
         hideBulletsOnMobile:"off",
         hideArrowsOnMobile:"off",
         hideThumbsUnderResoluition:0,
 
         hideThumbs:0,
         hideTimerBar:"off",
 
         keyboardNavigation:"off",
 
         navigationType:"thumb",
         navigationArrows:"solo",
         navigationStyle:"round",
         navigationVOffset: 46,
 
         touchenabled:"off",
         swipe_velocity:"0.7",
         swipe_max_touches:"1",
         swipe_min_touches:"1",
         drag_block_vertical:"false",
 
         parallax:"off",
         parallaxBgFreeze:"on",
         parallaxLevels:[10,7,4,3,2,5,4,3,2,1],
         parallaxDisableOnMobile:"off",
 
         stopAtSlide:-1,
         stopAfterLoops:-1,
         hideCaptionAtLimit:0,
         hideAllCaptionAtLilmit:0,
         hideSliderAtLimit:0,
 
         dottedOverlay:"none",
 
         spinned:"none",
 
         fullWidth:"off",
         forceFullWidth:"off",
         fullScreen:"off",
         fullScreenOffsetContainer:"#topheader-to-offset",
         fullScreenOffset:"0px",
 
         shadow:0
 
      });

      var hostname = $location.host();
      var port = $location.port();
      if(port != 80) {
        hostname = hostname+":"+port;
      }

      $('body').attr('style', '');

      $(".tp-thumbcontainer .bullet.thumb").each(function(index, thumb){
        var thumb_new_style = $(thumb).attr('style').replace(/url\(.*\)/,'url(http://'+hostname+''+ slides[index].image_thumb_url +')');
        $(thumb).attr('style', thumb_new_style);
      });

      var thumbnails_count = $(".tp-thumbcontainer .bullet.thumb").length;
      var thumbnails_container_width = (thumbnails_count * 200) + "px"

      $(".tp-thumbcontainer").css('width', thumbnails_count * 200);

      })(jQuery);


    };

    var js_height_init = function(){
      (function($){
          $(".js-height-full").height($(window).height());
          $(".js-height-parent").each(function(){
              $(this).height($(this).parent().first().height());
          });
      })(jQuery);
    }

    return {
      initPageSliders: initPageSliders,
      js_height_init: js_height_init,
    }

  }]);
