angular.module('myApp.page.services')
  .factory('Section', function(){

    var init = function(){
      
      var pageSection = $(".home-section, .page-section, .small-section, .split-section");
      pageSection.each(function(indx){
          
          if ($(this).attr("data-background")){
              $(this).css("background-image", "url(" + $(this).data("background") + ")");
          }
      });
      
      // Function for block height 100%
      function height_line(height_object, height_donor){
          height_object.height(height_donor.height());
          height_object.css({
              "line-height": height_donor.height() + "px"
          });
      }
      
      // Function equal height
      !function(a){
          a.fn.equalHeights = function(){
              var b = 0, c = a(this);
              return c.each(function(){
                  var c = a(this).innerHeight();
                  c > b && (b = c)
              }), c.css("height", b)
          }, a("[data-equal]").each(function(){
              var b = a(this), c = b.data("equal");
              b.find(c).equalHeights()
          })
      }(jQuery);


      var pageSection = $(".home-section, .page-section, .small-section, .split-section");
      pageSection.each(function(indx){
          
          if ($(this).attr("data-background")){
              $(this).css("background-image", "url(" + $(this).data("background") + ")");
          }
      });

    }

    return {
      init: init
    };

  });