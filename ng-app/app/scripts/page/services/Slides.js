angular.module('myApp.page.services')
  .factory('HomeSlides', ['$resource', function($resource){
    return $resource('/api/home_slides', null,
      {
        'get': { method: 'GET', isArray: true }
      }
    );
  }])
  .factory('CommercialSlides', ['$resource', function($resource){
    return $resource('/api/commercial_slides', null,
      {
        'get': { method: 'GET', isArray: true }
      }
    );
  }])
  .factory('ResidentialSlides', ['$resource', function($resource){
    return $resource('/api/residential_slides/:id', null,
      {
        'get': { method: 'GET', isArray: true }
      }
    );
  }]);