angular.module('myApp.page.services')
  .factory('PlatformDetect', function(){

    var mobileTest = function(){
      var mobileTest;
      if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
          mobileTest = true;
          $("html").addClass("mobile");
      }
      else {
          mobileTest = false;
          $("html").addClass("no-mobile");
      }
      return mobileTest;
    }

    var mozillaTest = function(){
      var mozillaTest;
      if (/mozilla/.test(navigator.userAgent)) {
          mozillaTest = true;
      }
      else {
          mozillaTest = false;
      }
      return mozillaTest;
    }

    var safariTest = function(){
      var safariTest;
      if (/safari/.test(navigator.userAgent)) {
          safariTest = true;
      }
      else {
          safariTest = false;
      }
      return safariTest;
    }

    var init = function() {      
      // Detect touch devices    
      if (!("ontouchstart" in document.documentElement)) {
          document.documentElement.className += " no-touch";
      }
    }

    return {
      mobileTest: mobileTest,
      mozillaTest: mozillaTest,
      safariTest: safariTest,
      init: init
    };

  });