angular.module('myApp.page.services')
  .factory('Team', function(){

    var init = function() {

      $(".team-item").click(function(){
        if ($("html").hasClass("mobile")) {
            $(this).toggleClass("js-active");
        }
      });
    }

    return { init: init };

  });