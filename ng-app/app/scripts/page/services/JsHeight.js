angular.module('myApp.page.services')
  .factory('JsHeight', function(){

    var init = function() {
      (function($){
          $(".js-height-full").height($(window).height());
          $(".js-height-parent").each(function(){
              $(this).height($(this).parent().first().height());
          });
      })(jQuery);
    }

    return { init: init };
  });