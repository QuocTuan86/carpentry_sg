angular.module('myApp.page.services')
  .factory('PeopleSay', function(){

    var init = function(){
      $(".single-carousel").owlCarousel({
        //transitionStyle: "backSlide",
        singleItem: true,
        autoHeight: true,
        navigation: true,
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
      });

      if ($(".owl-carousel").lenth) {
        var owl = $(".owl-carousel").data('owlCarousel');
        owl.reinit();
      }
    }

    return {
      init: init
    };

  });