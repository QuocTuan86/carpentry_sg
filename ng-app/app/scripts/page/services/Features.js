angular.module('myApp.page.services')
  .factory('Features', function(){

    var init = function(){
      $(".item-carousel").owlCarousel({
        autoPlay: 5000,
        //stopOnHover: true,
        items: 3,
        itemsDesktop: [1199, 3],
        itemsTabletSmall: [768, 3],
        itemsMobile: [480, 1],
        navigation: true,
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
      });

      if ($(".owl-carousel").lenth) {
        var owl = $(".owl-carousel").data('owlCarousel');
        owl.reinit();
      }
    }

    return {
      init: init
    };

  });