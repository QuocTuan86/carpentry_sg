angular.module('myApp.page.services')
  .factory('Counter', function(){

    var init = function(){

      $(".count-number").bind('inview', function(event, visible){
        var count = $(this);

        if(visible == true){
          count.countTo({
            from: 0,
            to: count.data('counter'),
            speed: 1300,
            refreshInterval: 60,
          });
        } else {

        }
      });
    }

    return {
      init: init
    };

  });