angular.module('myApp.page.filters', []).filter('dollarfy', ['$filter', function($filter){

  return function(amount){
      return $filter('currency')(amount, 'SGD$').replace(/\.00/, '');
  }
}]);