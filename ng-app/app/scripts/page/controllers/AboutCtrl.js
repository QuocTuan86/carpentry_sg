angular.module('myApp.page.controllers')
  .controller('AboutCtrl', [ '$scope',
                            'PlatformDetect',
                            'Section',
                            'Counter',
                            'JsHeight',
                            'ServiceHeight',
                            'Parallax',
                            'Service',
                            'Team',
                            'Features',
                            'PeopleSay',
                            'ScrollTop',
    function ($scope, PlatformDetect, Section, Counter, JsHeight,
              ServiceHeight, Parallax, Service, Team, Features,
              PeopleSay, ScrollTop) {

      $(window).resize(function(){
        JsHeight.init();
        ServiceHeight.init();
      });

      $(window).trigger("resize");
      PlatformDetect.init();
      Section.init();
      Counter.init();
      Parallax.init();
      Service.init();
      ServiceHeight.init();
      Team.init();
      Features.init();
      PeopleSay.init();
      ScrollTop.init();

      // Set the Map
      var set_google_map = function() {

        var gmMapDiv = $("#map-canvas");
        if (gmMapDiv.length) {
            var gmCenterAddress = gmMapDiv.attr("data-address");
            var gmMarkerAddress = gmMapDiv.attr("data-address");
            var gmColor = gmMapDiv.attr("data-color");
            
            map_cache = $(gmMapDiv.gmap3({
                action: "init",
                marker: {
                    address: gmMarkerAddress
                },
                map: {
                    options: {
                        zoom: 16,
                        zoomControl: true,
                        zoomControlOptions: {
                            style: google.maps.ZoomControlStyle.SMALL
                        },
                        mapTypeControl: false,
                        scaleControl: false,
                        scrollwheel: true,
                        streetViewControl: true,
                        draggable: true
                    }
                }
            }));
            return map_cache;
        };
      };

      // Show/Hide Map
      setTimeout(function(){
        var gmMapDiv = $("#map-canvas");
        $("#see-map").click(function(){ 
            $(this).toggleClass("js-active");
        
            if ($("html").hasClass("mobile")) {
                gmMapDiv.hide();
                gmMapDiv.gmap3({
                    action: "destroy"
                }).empty().remove();
            }
            else {
                gmMapDiv.slideUp(function(){
                    gmMapDiv.gmap3({
                        action: "destroy"
                    }).empty().remove();
                })
            }
        
            gmMapDiv.slideToggle(400, function(){
            
                if ($("#see-map").hasClass("js-active")) {
                    $(".google-map").append(gmMapDiv);
                    set_google_map();
                }
                
            });
            
            setTimeout(function(){
                $("html, body").animate({
                    scrollTop: $("#see-map").offset().top
                }, "slow", "easeInBack");
            }, 100);
            
            return false;
        });
      }, 1000);


    }

]);
