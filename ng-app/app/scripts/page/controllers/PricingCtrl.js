angular.module('myApp.page.controllers')
  .controller('PricingCtrl', ['$scope', '$sce', function ($scope, $sce) {
    
    $scope.items = [];
    $scope.totalPrice = function() { 
      return $scope.items.reduce( function(prev, curr) { 
        if (curr.itemPrice) {
          return prev + curr.itemPrice(); 
        }
        return prev;
      }, 0);
    }


    $scope.items.push({
      name: '<strong>Wardrobes</strong>',
      subsection: true
    });

    $scope.items.push({
      name: 'Full height Wardrobe (Swing door)',
      surfaceOptions: [ { name: 'Laminates', price: 240 }, { name: 'Veneer', price: 310 } ],
      quantityUnit: 'ft.',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: 'Full height Wardrobe (Normal sliding door)',
      surfaceOptions: [ { name: 'Laminates', price: 260 }, { name: 'Veneer', price: 330 } ],
      quantityUnit: 'ft.',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: 'Full height Open Wardrobe',
      surfaceOptions: [ { name: 'Polyken', price: 200 }, { name: 'Veneer', price: 330 } ],
      quantityUnit: 'ft.',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: '<strong>TV Consoles</strong>',
      subsection: true
    });

    $scope.items.push({
      name: 'TV Console (Low height)',
      surfaceOptions: [ { name: 'Laminates', price: 150 }, { name: 'Veneer', price: 220 } ],
      quantityUnit: 'ft.',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: 'Two Tier TV Console (Low height)',
      surfaceOptions: [ { name: 'Laminates', price: 180 }, { name: 'Veneer', price: 220 } ],
      quantityUnit: 'ft.',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: '<strong>Cabinets</strong>',
      subsection: true
    });

    $scope.items.push({
      name: 'Kitchen Cabinet',
      surfaceOptions: [ { name: 'Laminates', price: 120 }, { name: 'UV Finish', price: 130 }, { name: 'Glass Finish', price: 180 } ],
      quantityUnit: 'ft.',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: 'Storage/Display/Book Cabinet (Casement door)',
      surfaceOptions: [ { name: 'Laminates', price: 240 }, { name: 'UV Finish', price: 260 }, { name: 'Veneer', price: 310 } ],
      quantityUnit: 'ft.',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: 'Storage/Display/Book Cabinet (Open without door)',
      surfaceOptions: [ { name: 'Laminates', price: 330 }, { name: 'Veneer', price: 390 } ],
      quantityUnit: 'ft.',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: 'Low height Storage/Display/Recess Cabinet (Max height 36 inch., casement door)',
      surfaceOptions: [ { name: 'Laminates', price: 150 }, { name: 'UV Finish', price: 160 }, { name: 'Veneer', price: 200 } ],
      quantityUnit: 'ft.',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: 'Low height Open Storage/Display/Recess Cabinet (Max height 36 inch., without door)',
      surfaceOptions: [ { name: 'Laminates', price: 180 }, { name: 'Veneer', price: 260 } ],
      quantityUnit: 'ft.',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });
    $scope.items.push({
      name: 'Shoe Cabinet (Full Height)',
      surfaceOptions: [ { name: 'Laminates', price: 250 }, { name: 'UV Finish', price: 270 }, { name: 'Veneer', price: 320 } ],
      quantityUnit: 'ft.',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: 'Shoe Cabinet (Low Height)',
      surfaceOptions: [ { name: 'Laminates', price: 150 }, { name: 'UV Finish', price: 160 }, { name: 'Veneer', price: 220 } ],
      quantityUnit: 'ft.',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: 'Vanity cabinet',
      surfaceOptions: [ { name: 'Laminates', price: 150 }, { name: 'UV Finish', price: 160 } ],
      quantityUnit: 'ft.',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

     $scope.items.push({
      name: 'Kitchen Top (Basic Entry Range)',
      surfaceOptions: [ { name: 'Solid Surface', price: 75 }, { name: 'Granite', price: 115 }, { name: 'Marble', price: 165 } ],
      quantityUnit: 'ft.',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: '<strong>Tables</strong>',
      subsection: true
    });

    $scope.items.push({
      name: 'Suspended table (Without drawer)',
      surfaceOptions: [ { name: 'Laminates', price: 90 }, { name: 'Veneer', price: 150 } ],
      quantityUnit: 'ft.',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: 'Study table (with drawers)',
      surfaceOptions: [ { name: 'Laminates', price: 150 }, { name: 'Veneer', price: 200 } ],
      quantityUnit: 'ft.',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: 'Dressing table',
      surfaceOptions: [ { name: 'Laminates', price: 150 }, { name: 'Veneer', price: 200 } ],
      quantityUnit: 'ft.',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: '<strong>Walls</strong>',
      subsection: true
    });

    $scope.items.push({
      name: 'Feature Wall',
      surfaceOptions: [ { name: 'Laminates', price: 25 }, { name: 'Veneer', price: 30 } ],
      quantityUnit: 'ft.<sup>2</sup>',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: '<strong>Platform</strong>',
      subsection: true
    });

    $scope.items.push({
      name: 'Platform',
      surfaceOptions: [ { name: 'Plywood Finish', price: 15 }, { name: 'Laminate Finish', price: 22 } ],
      quantityUnit: 'ft.<sup>2</sup>',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: '<strong>Bedroom</strong>',
      subsection: true
    });

    $scope.items.push({
      name: '3ft Bed Frame (Single)',
      surfaceOptions: [ { name: 'Laminate', price: 750 } ],
      quantityUnit: 'set',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: '3.5ft Bed Frame (Super Single)',
      surfaceOptions: [ { name: 'Laminate', price: 800 } ],
      quantityUnit: 'set',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: '5ft Bed Frame (Queen Size)',
      surfaceOptions: [ { name: 'Laminate', price: 950 } ],
      quantityUnit: 'set',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: "6ft Bed Frame (King's)",
      surfaceOptions: [ { name: "Laminate", price: 1150 } ],
      quantityUnit: 'set',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: 'Pullout bedframe',
      surfaceOptions: [ { name: 'Laminate', price: 500 } ],
      quantityUnit: 'set',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: 'Bedside/Side table',
      surfaceOptions: [ { name: 'Laminates', price: 280 }, { name: 'Veneer', price: 330 } ],
      quantityUnit: 'set',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.push({
      name: 'Drawer unit (Every two drawers comes with third complementary drawer)',
      surfaceOptions: [ { name: 'Laminates', price: 250 }, { name: 'Veneer', price: 300 } ],
      quantityUnit: 'set',
      unitPrice: 0,
      quantity: 0,
      itemPrice: function(){ return this.unitPrice * this.quantity }
    });

    $scope.items.forEach(function(item){
      if( item.surfaceOptions ) {
        item.unitPrice = item.surfaceOptions[0].price;
      }
    });
}]);
