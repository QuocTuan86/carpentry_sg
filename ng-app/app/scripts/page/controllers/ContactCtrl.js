angular.module('myApp.page.controllers')
  .controller('ContactCtrl', [ '$scope',
                            'Section',
                            'JsHeight',
    function ($scope, Section, JsHeight) {

      $(window).trigger("resize");
      Section.init();

      $(window).resize(function(){
        JsHeight.init();
      });

      // Set the Map
      var set_google_map = function() {

        var gmMapDiv = $("#map-canvas");
        if (gmMapDiv.length) {
            var gmCenterAddress = gmMapDiv.attr("data-address");
            var gmMarkerAddress = gmMapDiv.attr("data-address");
            var gmColor = gmMapDiv.attr("data-color");
            
            map_cache = $(gmMapDiv.gmap3({
                action: "init",
                marker: {
                    address: gmMarkerAddress
                },
                map: {
                    options: {
                        zoom: 16,
                        zoomControl: true,
                        zoomControlOptions: {
                            style: google.maps.ZoomControlStyle.SMALL
                        },
                        mapTypeControl: false,
                        scaleControl: false,
                        scrollwheel: true,
                        streetViewControl: true,
                        draggable: true
                    }
                }
            }));
            return map_cache;
        };
      };
      setTimeout(set_google_map, 500);

    }

  ]);
