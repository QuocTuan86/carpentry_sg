angular.module('myApp.page.controllers')
  .controller('CommercialCtrl', ['$scope', 'PortfolioSliderInit', 'CommercialSlides', function ($scope, PortfolioSliderInit, CommercialSlides) {

    CommercialSlides.get().$promise.then(function(response){
      $scope.slides = response

      setTimeout(function(){
        PortfolioSliderInit.js_height_init();
        PortfolioSliderInit.initPageSliders($scope.slides);
      }, 500)
      
    });
    
    
  }]);