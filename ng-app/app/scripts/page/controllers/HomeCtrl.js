angular.module('myApp.page.controllers')
  .controller('HomeCtrl', ['$scope', 'HomeSliderInit', 'HomeSlides', function ($scope, HomeSliderInit, HomeSlides) {

    HomeSlides.get().$promise.then(function(response){
      $scope.slides = response

      setTimeout(function(){
        HomeSliderInit.js_height_init();
        HomeSliderInit.initPageSliders();
      }, 500)
      
    });
    
    
  }]);
