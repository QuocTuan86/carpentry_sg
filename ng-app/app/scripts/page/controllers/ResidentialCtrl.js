angular.module('myApp.page.controllers')
  .controller('ResidentialCtrl', ['$scope', 'PortfolioSliderInit', 'ResidentialSlides', function ($scope, PortfolioSliderInit, ResidentialSlides) {

    ResidentialSlides.get().$promise.then(function(response){
      $scope.slides = response

      setTimeout(function(){
        PortfolioSliderInit.js_height_init();
        PortfolioSliderInit.initPageSliders($scope.slides);
      }, 500)
      
    });
    
    
  }]);