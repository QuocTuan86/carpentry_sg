;var google_maps_loaded_def = null;
define(['jquery'],function($) {
  if(!google_maps_loaded_def) {
    google_maps_loaded_def = $.Deferred();
    window.google_maps_loaded = function() {
      google_maps_loaded_def.resolve(google.maps);   
      require(['scripts/scripts.js'],
        function(){
          $(".page-loader b").delay(0).fadeOut();
          $(".page-loader").delay(200).fadeOut("slow");
        },
        function(){})
    }
    require(['http://maps.googleapis.com/maps/api/js?sensor=true&language=en&callback=google_maps_loaded'],
      function(){},
      function(err) {
        google_maps_loaded_def.reject();
    });
  }
  return google_maps_loaded_def.promise();

});
