angular.module('myApp.navigation', ['myApp.navigation.controllers', 'myApp.navigation.services']);
angular.module('myApp.page', ['myApp.page.controllers', 'myApp.page.services', 'myApp.page.filters']);

angular
  .module('myApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'myApp.navigation',
    'myApp.page',
    'angulartics',
    'angulartics.google.analytics',
    'meta'
  ])
  .config(['$routeProvider', '$locationProvider', 'MetaProvider', function ($routeProvider, $locationProvider, MetaProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html'
      })
      .when('/contact', {
        templateUrl: '/views/contact.html'
      })
      .when('/portfolio-commercial', {
        templateUrl: '/views/commercial.html'
      })
      .when('/portfolio-residential', {
        templateUrl: '/views/residential.html'
      })
      .when('/pricing', {
        templateUrl: '/views/pricing.html'
      })
      .when('/how-it-works', {
        templateUrl: '/views/how-it-works.html'
      })
      .when('/information', {
        templateUrl: '/views/information.html'
      })
      .when('/terms', {
        templateUrl: '/views/terms.html'
      })
      .when('/about', {
        templateUrl: '/views/about.html'
      })
      .when('/career', {
        templateUrl: '/views/career.html'
      })
      .otherwise({
        redirectTo: '/'
      });

    MetaProvider
      .when('/', {
        title: 'Interior Design | Singapore Carpentry',
        description: {
          desc: 'We always innovate and design ever lasting appearance furniture to suit your interior design! We have experienced interior designers to design your carpentry!',
          keywords: 'singapore, carpentry, carpentry service, furniture singapore, interior design, design interior, building a house, cabinets, carpenter in singapore',
          canonical: 'http://www.carpentry.sg/'
        }
      })
      .when('/about', {
        title: 'About | Singapore Carpentry',
        description: {
          desc: 'We use a “design-and-build” concept; our experienced designers will work hand-in-hand with you to design entities precisely tailored to your requirements.',
          keywords: 'singapore, carpentry, carpentry service, interior design',
          canonical: 'http://www.carpentry.sg/about'
        }
      })
      .when('/portfolio-commercial', {
        title: 'Portfolio | Commercial | Singapore Carpentry',
        description: {
          desc: 'Past commercial projects include the following: Retail outlets, Offices, Tuition Centres. Portfolio of products that we custom made and designed to client\'s needs.',
          keywords: 'portfolio, commercial, Retail outlets, Offices, Tuition Centres',
          canonical: 'http://www.carpentry.sg/portfolio-commercial'
        }
      })
      .when('/portfolio-residential', {
        title: 'Portfolio | Residential | Singapore Carpentry',
        description: {
          desc: 'Portfolio of residential products that we custom made and designed to client\'s specifications and needs: Wardrobes, Bookshelves and Dressing Tables.',
          keywords: 'portfolio, residential, Wardrobes, Bookshelves, Aesthetic displays, Dressing Tables',
          canonical: 'http://www.carpentry.sg/portfolio-residential'
        }
      })
      .when('/pricing', {
        title: 'Pricing | Singapore Carpentry',
        description: {
          desc: 'We understand the importance of fair pricing. That is why here at Singapore Carpentry, we are always looking to help you save costs and work within your budget.',
          keywords: 'Prices for Carpenter, carpenter in singapore',
          canonical: 'http://www.carpentry.sg/pricing'
        }
      })
      .when('/how-it-works', {
        title: 'How it works | Singapore Carpentry',
        description: {
          desc: 'To get started, it\'s easy. Call us or drop us an email. Here\'s an overview of how it works. At Singapore Carpentry, making your dream home is just that simple!',
          keywords: 'Singapore Carpentry, Enquiry, Quotation, Accept, Design & Measure, Confirm, Delivery, Complete',
          canonical: 'http://www.carpentry.sg/how-it-works'
        }
      })
      .when('/information', {
        title: 'More information | Singapore Carpentry',
        description: {
          desc: 'Singapore Carpentry prides itself on quality! In order to help you make an informed choice and facilitate the deciding process, do take note of the following.',
          keywords: 'carpentry service, Pricing information',
          canonical: 'http://www.carpentry.sg/information'
        }
      })
      .when('/terms', {
        title: 'Terms and conditions | Singapore Carpentry',
        description: {
          desc: 'By engaging us to perform the works described above, you will also be deemed to have accepted these terms and conditions as set out below.',
          keywords: 'carpentry service, Terms and conditions',
          canonical: 'http://www.carpentry.sg/terms'
        }
      })
      .when('/contact', {
        title: 'Contact | Singapore Carpentry',
        description: {
          desc: 'At Singapore Carpentry, making your dream home is just that simple! For a free quote, showroom visits or any enquiries, do not hesitate to contact us!',
          keywords: 'carpentry service, furniture singapore, interior design, design interior, building a house, cabinets, carpenter in singapore',
          canonical: 'http://www.carpentry.sg/contact'
        }
      })
      .when('/career', {
        title: 'Career | Singapore Carpentry',
        description: {
          desc: 'Join the Singapore Carpentry team! We welcome people who share our vision of contributing and value-adding to the community by providing quality carpentry.',
          keywords: 'Carpenter, Carpentry Careers, Carpentry Jobs',
          canonical: 'http://www.carpentry.sg/career'
        }
      })
      .otherwise({
        title: 'Interior Design | Singapore Carpentry',
        description: {
          desc: 'We always innovate and design ever lasting appearance furniture to suit your interior design! We have experienced interior designers to design your carpentry!',
          keywords: 'singapore, carpentry, carpentry service, furniture singapore, interior design, design interior, building a house, cabinets, carpenter in singapore',
          canonical: 'http://www.carpentry.sg/'
        }
      })

    // use the HTML5 History API
    $locationProvider.html5Mode(true);
  }])
  .run(['Meta', function(Meta){
    Meta.init();
  }]);
