role :app, %w{ 128.199.129.73 }
role :web, %w{ 128.199.129.73 }
role :db,  %w{ 128.199.129.73 }

server '128.199.129.73',
  user: 'carpentry',
  roles: %w{web app db},
  ssh_options: {
    user: 'carpentry',
    keys: %w(~/.ssh/carpentry),
    forward_agent: false,
    auth_methods: %w(publickey)
  }
