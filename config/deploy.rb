# config valid only for Capistrano 3.1
lock '3.2.1'


set :scm, :git
set :branch, 'master'
set :application, 'carpentry_sg'
set :repo_url, 'git@bitbucket.org:kennethlimjf/carpentry_sg.git'
set :deploy_to, "/home/carpentry/carpentry_sg"
set :keep_releases, 3
set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do

  desc "Upload local_env.yml containing environment variables"
  task :upload_keys do
    on roles(:app) do
      execute "rm -rf #{shared_path}/config/local_env.yml"
      upload! StringIO.new(File.read("config/local_env.yml")), "#{shared_path}/config/local_env.yml"
    end
  end

  desc "Symlink local_env.yml file"
  task :symlink_keys do
    on roles(:app) do
      execute "ln -sfn #{shared_path}/config/local_env.yml #{release_path}/config/local_env.yml"
    end
  end

  desc "Symlink shared/public/system(uploads) folder with rails_app/public/system folder"
  task :symlink_images do
    on roles(:app) do
      execute "rm -rf #{release_path}/public/system"
      execute "ln -nfs #{shared_path}/public/system #{release_path}/public/system"
    end
  end

  desc 'Stop Puma Server'
  task :stop_server do
    on roles(:app) do
      execute "kill `cat #{shared_path}/tmp/pids/puma.pid`"
    end
  end

  desc 'Start Puma Server'
  task :start_server do
    on roles(:app) do
      execute "cd #{release_path}; ( RBENV_ROOT=~/.rbenv RBENV_VERSION=2.1.3 RAILS_ENV=production RBENV_ROOT=~/.rbenv RBENV_VERSION=2.1.3 ~/.rbenv/bin/rbenv exec bundle exec puma -C config/puma.rb )"
    end
  end

  after :updating, :upload_keys
  after :upload_keys, :symlink_keys
  after :symlink_keys, :stop_server
  after :stop_server, :symlink_images
  after :publishing, :start_server

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end
