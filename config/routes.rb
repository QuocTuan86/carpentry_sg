Rails.application.routes.draw do
  root to: redirect('/')
  get '/about', to: redirect('#/about')
  get '/portfolio-commercial', to: redirect('#/portfolio-commercial')
  get '/portfolio-residential', to: redirect('#/portfolio-residential')
  get '/pricing', to: redirect('#/pricing')
  get '/how-it-works', to: redirect('#/how-it-works')
  get '/information', to: redirect('#/information')
  get '/terms', to: redirect('#/terms')
  get '/contact', to: redirect('#/contact')
  get '/career', to: redirect('#/career')

  devise_for :users, class_name: "Admin::User"

  namespace :api do
    resources :home_slides, only: [:index]
    resources :commercial_slides, only: [:index]
    resources :residential_slides, only: [:index]
  end
  
  namespace :admin do
    get '/', to: redirect('/admin/dashboard')
    get '/dashboard', to: 'dashboard#index', as: :dashboard

    resources :home_slides, only: [:index, :new, :create]
    resources :commercial_slides, only: [:index, :new, :create]
    resources :residential_slides, only: [:index, :new, :create]

    namespace :api do
      # Routes for Admin Home Slides
      resources :home_slides, only: [:index, :destroy]
      put '/home_slides', to: 'home_slides#update_order'

      # Routes for Admin Commercial Slides
      resources :commercial_slides, only: [:index, :destroy]
      put '/commercial_slides', to: 'commercial_slides#update_order'

      # Routes for Admin Residential Slides
      resources :residential_slides, only: [:index, :destroy]
      put '/residential_slides', to: 'residential_slides#update_order'
    end
  end
end