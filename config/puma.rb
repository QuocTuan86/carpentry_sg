threads 1,16
workers 2
preload_app!
environment 'production'
daemonize true
pidfile '/home/carpentry/carpentry_sg/shared/tmp/pids/puma.pid'
state_path '/home/carpentry/carpentry_sg/shared/tmp/pids/puma.state'
stdout_redirect '/home/carpentry/carpentry_sg/shared/log/stdout', '/home/carpentry/carpentry_sg/shared/log/stderr', true
bind 'unix:///home/carpentry/carpentry_sg/shared/tmp/sockets/carpentry-puma.sock'