module Api
  class HomeSlidesController < Api::ApiController

    def index
      @slides = HomeSlide.all.order(:rank)
      render 'admin/shared/slides'
    end
    
  end
end