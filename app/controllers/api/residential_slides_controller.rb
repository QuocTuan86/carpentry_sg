module Api
  class ResidentialSlidesController < Api::ApiController

    def index
      @slides = ResidentialSlide.all.order(:rank)
      render 'admin/shared/slides'
    end
    
  end
end