module Api
  class CommercialSlidesController < Api::ApiController

    def index
      @slides = CommercialSlide.all.order(:rank)
      render 'admin/shared/slides'
    end
    
  end
end