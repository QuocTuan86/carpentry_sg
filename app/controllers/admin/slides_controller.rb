module Admin
  class SlidesController < Admin::AdminController

    # Macros
    def self.set_resource(resource_name)
      define_method(:resource) do 
        "#{resource_name.to_s}" || 'home_slide' # Default value
      end
    end


    # Helper Methods
    helpers = %w(slide_type index_path new_path resource resource_model)
    hide_action(*helpers)
    helper_method(*helpers)

    def slide_type
      resource.split("_").first.capitalize
    end

    def index_path
      "/admin/#{resource}s"
    end

    def new_path
      "/admin/#{resource}s/new"
    end

    def resource_model
      eval(resource.camelize)
    end



    # Controller Actions
    def index
      render 'admin/shared/index_slides'
    end

    def new
      @slide = resource_model.new
      render 'admin/shared/new_slide'
    end

    def create
      @slide = resource_model.new(slide_params)
      @slide.set_rank

      if @slide.save
        flash[:notice] = "Sucessful."
        redirect_to index_path
      else
        flash[:error] = "There is a problem with your upload. Please try again."
        render :new
      end
    end

    private
      def slide_params
        params.require(resource.to_sym).permit!
      end

  end
end