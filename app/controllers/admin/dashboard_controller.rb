module Admin
  class DashboardController < Admin::AdminController
    def index
      @home_slides_count = ::HomeSlide.all.count
      @commercial_slides_count = ::CommercialSlide.all.count
      @residential_slides_count = ::ResidentialSlide.all.count
    end
  end
end