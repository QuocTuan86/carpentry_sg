module Admin
  module Api
    class SlidesController < Admin::Api::ApiController

      # Macros
      def self.set_resource(resource_name)
        define_method(:resource) do 
          "#{resource_name.to_s}" || 'home_slide' # Default value
        end
      end

      def resource_model
        eval(resource.camelize)
      end

      # Controller Methods
      def index
        @slides = resource_model.all.order(:rank)
        render 'admin/shared/slides'
      end

      def update_order
        if params[:_json]
          slides = params.require(:_json)

          if resource_model.update_all_ranks(slides)
            render json: { success: true }
          else
            render json: { success: false }
          end
        else
          render json: { success: true }
        end
      end

      def destroy
        slide = resource_model.find_by(id: params.require(:id))
        if slide.destroy
          render json: { success: true }
        else
          render json: { success: false }
        end
      end
    end
  end
end