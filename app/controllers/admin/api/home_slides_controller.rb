module Admin
  module Api
    class HomeSlidesController < Admin::Api::SlidesController
      set_resource :home_slide
    end
  end
end