module Admin
  module Api
    class CommercialSlidesController < Admin::Api::SlidesController
      set_resource :commercial_slide
    end
  end
end