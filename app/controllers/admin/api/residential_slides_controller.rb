module Admin
  module Api
    class ResidentialSlidesController < Admin::Api::SlidesController
      set_resource :residential_slide
    end
  end
end