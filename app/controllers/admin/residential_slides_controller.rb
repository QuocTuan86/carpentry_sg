module Admin
  class ResidentialSlidesController < Admin::SlidesController
    set_resource :residential_slide
  end
end