module Admin
  class HomeSlidesController < Admin::SlidesController
    set_resource :home_slide
  end
end