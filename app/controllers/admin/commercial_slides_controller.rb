module Admin
  class CommercialSlidesController < Admin::SlidesController
    set_resource :commercial_slide
  end
end