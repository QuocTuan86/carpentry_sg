angular.module('sortableApp.controllers', [])
  .controller('MainCtrl', ['$scope', 'HomeSlides', 'CommercialSlides', 'ResidentialSlides', '$timeout',
    function($scope, HomeSlides, CommercialSlides, ResidentialSlides, $timeout){

      var ResourceModel = eval($('#slide-model').val());

      $scope.refreshSlides = function(){
        ResourceModel.get().$promise.then(function(response){
          response.forEach(function(slide){ slide.old_rank = slide.rank });
          $scope.slides = response;
        });
      }

      $scope.rerank = function(){
        $scope.slides.map(function(slide, index){ slide.rank = index + 1; });
      }
      $scope.sortableOptions = {
        update: function(e, ui) {
          $timeout($scope.rerank, 150);
        }
      };
      $scope.updateOrder = function() {

        var slidesForUpdate = [];
        $scope.slides.forEach(function(slide){
          if(slide.rank != slide.old_rank) slidesForUpdate.push(slide);
        });

        ResourceModel.update(slidesForUpdate)
          .$promise.then(function(response){
            if (response && response.success) {
              alert("Updated successfully.");
            } else {
              alert("There's an error in an attempt to update the order");
            }
            $scope.refreshSlides();
          });
      }
      $scope.destroySlide = function(slide){
        ResourceModel.destroy({ id: slide.slide_id })
          .$promise.then(function(response){
            if (response && response.success) {
              var idx;
              $scope.slides.forEach(function(item, index){ 
                if(item.slide_id == slide.slide_id) {
                  idx = index
                }
              });
              $scope.slides.splice(idx, 1);
            } else {
              alert("There's an error in an attempt to delete the photo");
            }
          });
      }

      $scope.refreshSlides();

    }]);