angular.module('sortableApp', [ 'ngResource',
                                'ui.sortable',
                                'sortableApp.controllers',
                                'sortableApp.services'
]).
config(['$httpProvider', function($httpProvider) {
  // Get Auth Token
  var authToken = $('meta[name=\"csrf-token\"]').attr("content");
  $httpProvider.defaults.headers.common["X-CSRF-TOKEN"] = authToken
}]);