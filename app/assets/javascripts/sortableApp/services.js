angular.module('sortableApp.services',[])
  .factory('HomeSlides', ['$resource', function($resource){
    return $resource('/admin/api/home_slides/:id', null,
      {
        'get': { method: 'GET', isArray: true },
        'update': { method: 'PUT' },
        'destroy': { method: 'DELETE' }
      }
    );
  }])
  .factory('CommercialSlides', ['$resource', function($resource){
    return $resource('/admin/api/commercial_slides/:id', null,
      {
        'get': { method: 'GET', isArray: true },
        'update': { method: 'PUT' },
        'destroy': { method: 'DELETE' }
      }
    );
  }])
  .factory('ResidentialSlides', ['$resource', function($resource){
    return $resource('/admin/api/residential_slides/:id', null,
      {
        'get': { method: 'GET', isArray: true },
        'update': { method: 'PUT' },
        'destroy': { method: 'DELETE' }
      }
    );
  }]);