json.array! @slides do |slide|
  json.slide_id slide.id
  json.file_name slide.image_file_name.split(".").first
  json.rank slide.rank
  json.image_thumb_url slide.image.url(:thumb)
  json.image_screen_url slide.image.url(:screen)
  json.image_original_url slide.image.url(:original)
  json.created_at datetime_formatter(slide.created_at)
end