module ApplicationHelper
  def datetime_formatter(datetime)
    if datetime
      time_zone = current_user.try(:time_zone) || Time.zone.name
      datetime.in_time_zone(time_zone).strftime("%d %b %Y,%l:%M %p (%Z%z)")  
    end
  end

  def menu_active(path)
    return 'active' if request.path == path
  end
end
