module Rankable
  extend ActiveSupport::Concern

  def set_rank
    @total_count = self.class.all.count
    self.rank = @total_count + 1
  end

  module ClassMethods
    def update_all_ranks(slides)
      slides.each do |slide|
        record = self.find_by(id: slide["slide_id"])
        record.rank = slide["rank"]
        record.save
      end
    end
  end
end