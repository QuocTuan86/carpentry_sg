module Imageable
  extend ActiveSupport::Concern

  included do
    has_attached_file :image, :styles => { :thumb => "200x150#", :screen => "1920x910#" }, :default_url => "/images/:style/missing.png"
    validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  end
end