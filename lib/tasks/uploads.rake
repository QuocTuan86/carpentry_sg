namespace :uploads do
  desc "Restore file upload"

  task :restore do
    `cp -r ./system ./public/system`
    puts "Uploads folder (system) restored."
  end
end
