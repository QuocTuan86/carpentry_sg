namespace :ng do
  desc "Build ng-app"
  task :build do
    puts `cd ng-app; grunt build --force`
    puts "ng-app build complete."
    Rake::Task["uploads:restore"].invoke
  end

  desc "Gzip assets"
  task :compress do
    files = Dir["public/**/*.js", "public/**/*.css"]
    files.each do |file|
      command = "gzip -c #{file} > #{file}.gz"
      `#{command}`
    end
  end
end
