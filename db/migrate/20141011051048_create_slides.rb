class CreateSlides < ActiveRecord::Migration
  def up
    create_table :home_slides do |t|
      t.integer :rank
      t.timestamps
    end
    add_attachment :home_slides, :image
    add_index :home_slides, :rank

    create_table :commercial_slides do |t|
      t.integer :rank
      t.timestamps
    end
    add_attachment :commercial_slides, :image
    add_index :commercial_slides, :rank

    create_table :residential_slides do |t|
      t.integer :rank
      t.timestamps
    end
    add_attachment :residential_slides, :image
    add_index :residential_slides, :rank
  end

  def down
    drop_table :home_slides
    drop_table :commercial_slides
    drop_table :residential_slides
  end
end
